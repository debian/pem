pem (0.7.9-6) unstable; urgency=medium

  * Included missing upstream tarball signature among changes.
  * debian/control:
      - Bumped Standards-Version to 4.6.2.
      - Updated maintainer email address.
  * debian/copyright: updated packaging copyright years.

 -- David da Silva Polverari <polverari@debian.org>  Thu, 02 Nov 2023 20:31:42 +0000

pem (0.7.9-5) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.1.
  * debian/copyright: updated packaging copyright years.

 -- David da Silva Polverari <david.polverari@gmail.com>  Mon, 05 Dec 2022 21:05:27 +0000

pem (0.7.9-4) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.0.
  * debian/examples: added to install example file to the right path.
  * debian/patches/010_use-usr-bin-perl.patch: added 'Forwarded: not-needed' to
    the patch header, as it is a Debian-specific change.
  * debian/rules: overrode dh_installexamples to remove example file from
    /usr/share/pem/ after installation.
  * debian/tests/:
      - balance: created to provide a non-superficial functional test.
      - control: added a test stanza to run the 'balance' test.

 -- David da Silva Polverari <david.polverari@gmail.com>  Sun, 07 Nov 2021 06:04:02 +0000

pem (0.7.9-3) unstable; urgency=medium

  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added '${perl:Depends}' to Depends field.
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added Vcs-* fields.
      - Bumped Standards-Version to 4.5.0.
      - Marked pem as 'Multi-Arch: foreign'.
      - Removed redundant dh-autoreconf build dependency.
  * debian/copyright: updated copyright years.
  * debian/patches/010_use-usr-bin-perl.patch: added to use '/usr/bin/perl'
    instead of '/usr/bin/env perl' for interpreter invocation.
  * debian/rules: removed redundant '--with autoreconf' dh parameter.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: added to perform a trivial CI test.
  * debian/upstream/metadata: created.
  * debian/watch: using a secure URI.

 -- David da Silva Polverari <david.polverari@gmail.com>  Tue, 16 Jun 2020 17:30:37 -0500

pem (0.7.9-2) unstable; urgency=medium

  * Fix project homepage address (Closes: #830177)

 -- David da Silva Polverari <david.polverari@gmail.com>  Mon, 02 Apr 2018 22:31:10 -0300

pem (0.7.9-1) unstable; urgency=medium

  * Initial release (Closes: #500100)

 -- David da Silva Polverari <david.polverari@gmail.com>  Sat, 30 Apr 2016 00:39:00 -0300
